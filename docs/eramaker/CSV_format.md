# eramakerのCSV書式
**当ページはeramakerの情報について記載しています。現行のEmueraとは仕様が違う部分があります**

## 基本情報
### CSVファイルについて
eramaker.exeの直下に、CSVという名前のフォルダをおきます。  
CSVファイルには、以下のようなものがあります。  

|ファイル名|仕様|
|:--|:--|
|GameBase.csv|ゲームの基本的データを登録します。|
|Palam.csv|調教中パラメータ（快Ｃ、苦痛、反感など）を登録します。|
|Abl.csv|能力（Ｖ感覚、奉仕精神、マゾっ気など）を登録します。|
|Talent.csv|素質（臆病、自制心、回復早いなど）を登録します。|
|Mark.csv|刻印（快楽刻印、恥辱刻印など）を登録します。|
|Exp.csv|経験（Ｖ経験、自慰経験など）を登録します。|
|Train.csv|調教コマンド（クンニ、フェラチオ、鞭など）を登録します。|
|Item.csv|アイテム（バイブ、針など）を登録します。|
|Str.csv|ゲーム中で使う色々な文章を登録します。|
|CharaXX.csv|キャラの初期データを登録します。Emueraでは任意の数字で指定可能|

### CSVファイルの書き方
すべてのCSVファイルにおいて、  

- 1列目の1文字目が半角の;（セミコロン）だった場合、その行は無視されます。また、空行も無視されます。

		例
		;体力と気力の設定
		基礎,0,2000
		基礎,1,1000

		;能力の設定
		能力,0,2

* 数値を入力するときは半角にしてください。

	**正**
		
		121,ふたなり
	**誤**

		１２１,ふたなり

* 文字列を""でくくると正常に動作しません。OpenOfficeなどでは初期設定でこれになっているようです。

	**正**

		0,従順
		1,欲望
		2,技巧
	**誤**

		0,"従順"
		1,"欲望"
		2,"技巧"
設定方法はお使いの表計算ソフトのヘルプを参照してください。

## 各ファイルの書式

### `GameBase.csv`の書式
1列目に命令を、2列目以降にデータを書きます。

- コード,(数値)
    - ゲームコードを(数値)に設定します。これは、誤って別のゲームのセーブデータをロードしてしまうことを防ぐために使います。(数値)は適当な値でかまいません。

- バージョン,(数値)
	- ゲームのバージョンを(数値)に設定します。画面上は、(数値)を1000で割ったものが表示されます（100なら0.10）。初期状態では、違うバージョンで保存されたセーブデータは読み込まないようになっています。

- タイトル,(文字列)
	- ゲームのタイトルを(文字列)に設定します。起動時に表示されます。

- 作者,(文字列)
	- ゲームの作者を(文字列)に設定します。起動時に表示されます。

- 製作年,(文字列)
	- ゲームの製作年を(文字列)に設定します。起動時に表示されます。数値ではないので2005-2006などの表記も可能です。

- 追加情報,(文字列)
	- ゲームの追加情報を(文字列)に設定します。起動時に表示されます。

- 最初からいるキャラ,(数値)
	- ゲーム開始時に、主人公以外でいるキャラを設定します。era lightのように、最初から調教するキャラが決まっているゲームで使います。(数値)にはキャラ番号を指定します。たとえば1を指定すればChara01.csvを、12を指定すればChara12.csvを見に行きます。

- アイテムなし,(数値)
	- (数値)を1にすると、バイブなどの調教時にアイテムが必要な調教でも、アイテムなしで調教できるようにします。アイテムの概念がないゲームを作るとき、1にしてください。
バージョン違い認める,(数値)

- セーブデータのバージョンが(数値)以上の場合、バージョンが違っても読み込めるようにします。全体に影響を及ぼさないバージョンアップをした場合、これを設定してください。

	**例**

		;バージョン1.20より後のセーブデータは読み込む
		バージョン違い認める,1200

### Palam.csvの書式
- 1列目にパラメータ番号を、2列目にパラメータ名を書きます。
- パラメータ番号の数値は0からはじめ、空き番号は作らないことを推奨します。
- パラメータ番号は最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Abl.csvの書式
- 1列目に能力番号を、2列目に能力名を書きます。
- 能力番号の数値は0からはじめ、空き番号は作らないことを推奨します。
- 能力番号は最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Talent.csvの書式
- 1列目に素質番号を、2列目に素質名を書きます。
- 空き番号を作ってもかまいません。
- 素質番号は最低で0、最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Mark.csvの書式
- 1列目に刻印番号を、2列目に刻印名を書きます。
- 刻印番号の数値は0からはじめ、空き番号は作らないことを推奨します。
- 刻印番号は最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Exp.csvの書式
- 1列目に経験番号を、2列目に経験名を書きます。
- 空き番号を作ってもかまいません。
- 経験番号は最低で0、最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Train.csvの書式
- 1列目にコマンド番号を、2列目にコマンド名を書きます。
- 空き番号を作ってもかまいません。
- コマンド番号は最低で0、最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Item.csvの書式
- 1列目にアイテム番号を、2列目にアイテム名を、3列名にアイテムの価格を書きます。
- 空き番号を作ってもかまいません。
- アイテム番号は最低で0、最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### Str.csvの書式
- 1列目に文字列番号を、2列目に文字列を書きます。
- いちおう文字列の長さに制限はありません。
- 空き番号を作ってもかまいません。
- 文字列番号は最低で0、最高で19999でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

### CharaXX.csvの書式
- 番号,(数値)
	- キャラ番号を(数値)に設定します。キャラ同士の相性、同一キャラのスペシャルバージョンなどを作るときに重要です。

- 名前,(文字列)
- キャラの名前を(文字列)に設定します。いちおう文字列の長さに制限はありませんが、長すぎると表示が乱れることが考えられます。

- 呼び名,(文字列)
	- キャラの呼び名を(文字列)に設定します。あだ名のあるキャラなどに使いましょう。
	- いちおう文字列の長さに制限はありませんが、長すぎると表示が乱れることが考えられます。

- 基礎,(数値1),(数値2)
	- キャラの基礎パラメータの(数値1)番目を(数値2)に設定します。
	- サンプルゲームでは0番目が体力、1番目が気力、2番目が射精ゲージになっています。
	- (数値1)は最低で0、最高で99でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に

	**例**

		;体力が２０００で気力が１０００
		基礎,0,2000
		基礎,1,1000

- 能力,(数値1),(数値2)
	- キャラの初期能力の(数値1)番目を(数値2)に設定します。
	- (数値1)にはAbl.csvで指定した能力番号を使ってください。
	- (数値2)に特に制限はありませんが、サンプルゲームでは0から5までとしています。

- 素質,(数値)
	- キャラの素質に(数値)番目のものを加えます。
	- (数値)にはTalent.csvで指定した素質番号を使ってください。

- 経験,(数値1),(数値2)
	- キャラの初期経験の(数値1)番目を(数値2)に設定します。
	- (数値1)にはExp.csvで指定した経験番号を使ってください。

- 相性,(数値1),(数値2)
	- (数値1)番目のキャラに対する相性を(数値2)に設定します。
	- (数値1)にはCharaXX.csvでそれぞれ指定したキャラ番号を使ってください。
	- (数値2)は100が標準で、それより低いと相性が悪い、高いと相性がよいという意味になります。

- 助手,(数値)
	- (数値)を1にすると、初期状態から助手として扱われます。

- フラグ,(数値1),(数値2)
	- (数値1)番目のキャラフラグを(数値2)に設定します。
	-  (数値1)は最低で0、最高で999でしたが、Emueraでは`VariableSize.csv`で上限の変更が可能に
	- キャラフラグはゲーム製作者のアイデア次第で自由に使えます。サンプルゲームでは、0番目のフラグが1であるキャラは「スペシャルキャラ」であるとしています。
	- （※フラグという名前ですが、整数の値なら0と1以外でも大丈夫です）

## 知っておくと便利なこと
- Str.csvでの***や+++について
- サンプルゲームera lightのStr.csvには、***や+++などが多く出てきます。ゲーム内ではそれぞれ「あかり」「浩之ちゃん」と変換されているはずです。
- これを完全に理解するにはera basicの知識が必要ですが、ひとまず***は調教しているキャラの名前、+++は主人公の呼び名、$$$は調教しているキャラの呼び名だと知っていればよいでしょう。
- サンプルゲームerakanonでは助手システムがあります。///は助手の名前、===はそのとき調教している人（主人公or助手）の呼び名に変換されます。
