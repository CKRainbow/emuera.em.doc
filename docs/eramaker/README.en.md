---
hide:
  - toc
---

# Information about eramaker
Most of the features implemented in eramaker are not explained in Emuera's documentation. Therefore if you need, check eramaker's below:  

- [Eramaker Game Structure](system_flow.en.md)
- [Eramaker CSV File Format](CSV_format.en.md)
- [Eramaker ERB File Format](ERB_format.en.md)
- [Eramaker Variable List](variables.en.md)

You can also refer to the following link for the original japanese version: [http://cbaku.com/b/2010/12/eramaker/](http://cbaku.com/b/2010/12/eramaker/)
