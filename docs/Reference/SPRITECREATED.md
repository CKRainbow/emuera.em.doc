---
hide:
  - toc
---

# SPRITECREATED

| 関数名                                                                     | 引数     | 戻り値 |
| :------------------------------------------------------------------------- | :------- | :----- |
| ![](../assets/images/IconEmuera.webp)[`SPRITECREATED`](./SPRITECREATED.md) | `string` | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int SPRITECREATED spriteName
    ```
    指定した名称のスプライトが作成済みであるなら1を、未作成又は廃棄済みであるなら0を返します。

!!! hint "ヒント"

    命令、式中関数両方対応しています。
