---
hide:
  - toc
---

# QUIT

| 関数名                                                     | 引数 | 戻り値 |
| :--------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/Iconeramaker.webp)[`QUIT`](./QUIT.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	QUIT
    ```
    [`WAIT`](./WAIT.md)を1回挟んで`Emuera`を終了する


!!! hint "ヒント"

    命令のみ対応しています。
