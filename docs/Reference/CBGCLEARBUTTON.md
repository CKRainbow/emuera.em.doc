---
hide:
  - toc
---

# CBGCLEARBUTTON

| 関数名                                                                       | 引数 | 戻り値 |
| :--------------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`CBGCLEARBUTTON`](./CBGCLEARBUTTON.md) | なし | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int CBGCLEARBUTTON
    ```
	[`CBGSETBUTTONSPRITE`](./CBGSETBUTTONSPRITE.md)命令で設定したボタンの設定を解除します。

!!! hint "ヒント"

    命令、式中関数両方対応しています。
