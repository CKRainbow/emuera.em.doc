---
hide:
  - toc
---

# CBGCLEAR

| 関数名                                                           | 引数 | 戻り値 |
| :--------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`CBGCLEAR`](./CBGCLEAR.md) | なし | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int CBGCLEAR
    ```
	`CBG`で始まる各種`CBG`系命令で設定した背景画像の設定を全て解除します。

!!! hint "ヒント"

    命令、式中関数両方対応しています。
