---
hide:
  - toc
---

# SKIPLOG

| 関数名                                                     | 引数  | 戻り値 |
| :--------------------------------------------------------- | :---- | :----- |
| ![](../assets/images/IconEE.webp)[`SKIPLOG`](./SKIPLOG.md) | `int` | `void` |

!!! info "API"

	``` { #language-erbapi }
	SKIPLOG bool
	```

	左クリック等でのログスキップの状態を任意で操作する。非0でスキップ、0でスキップ状態を解除

!!! hint "ヒント"

    命令としてのみ使用可能
