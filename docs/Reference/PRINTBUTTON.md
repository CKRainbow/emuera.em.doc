---
hide:
  - toc
---

# PRINTBUTTON系

| 関数名                                                                        | 引数           | 戻り値 |
| :---------------------------------------------------------------------------- | :------------- | :----- |
| ![](../assets/images/IconEmuera.webp)[`PRINTBUTTON(|C|LC)`](./PRINTBUTTON.md) | `string`, `any`| なし   |

!!! info "API"

    ```  { #language-erbapi }
	PRINTBUTTON(|C|LC) string, buttonValue
    ```
	`PRINTBUTTON`命令はマウスでクリックできるボタンを生成するための命令です。  
	書式としては[`PRINTS`](./PRINT.md)命令に近いですが、第二引数としてクリックされたときに入力する数字または文字を指定します。 第一引数に改行コードが入っている場合オミットされ改行されません。  

	Emueraは`[300] セーブ`のように`[]`でくくられた数字とその前後の文字列を自動的にボタンに変換します。  
	このようなボタンを自動ではなく強制的に生成するための命令が`PRINTBUTTON`です。  
	この命令は例えば以下のような場合に有用です。  

    ```  { #language-erbapi }
	PRINT これでいい？ [0] はい    [1] いいえ  
	INPUT  
	```

	このような行についてはEmueraはボタンを正しく認識できず、`これでいい？ [0] はい`というボタンと`[1] いいえ`というボタンになってしまいます。  
	`PRINTBUTTON`を用いて書き直すと以下のようになります。  

    ```  { #language-erbapi }
	PRINTS "これでいい？ "  
	PRINTBUTTON "[0] はい", 0  
	PRINTS "     "  
	PRINTBUTTON "[1] いいえ", 1  
	INPUT  
	```

	（`PRINT`命令の代わりに`PRINTS`を使っていますがこれは半角スペースの数を明らかにするためです）  
	このようにすると`これでいい？ `はボタンではなくなり、`[0] はい`と`[1] いいえ`だけがボタンになります。  
	なお、`PRITNBUTTON`命令では表示する文字列に`[0]`や`[1]`などが含まれていることは必須ではありませんが、対応する数字を全く表示しないとテンキーなどで操作している方を戸惑わせることになるでしょう。 従来通り`[0]`などを表記することを勧めます。  
	また、`PRINTBUTTON`命令では数字だけでなく文字列を入力するボタンを作成することができます。 そうして作成したボタンは[`INPUTS`](./INPUT.md)命令の実行時にクリックすることができます。  

    ```  { #language-erbapi }
	PRINTL 名前を入力してください。  
	PRINTBUTTON "[ほげほげ] ", "ほげほげ"  
	PRINTBUTTON "[ぷげぷげ] ", "ぷげぷげ"  
	PRINTBUTTON "[ふうばあ] ", "ふうばあ"  
	INPUTS  
	```

	括弧内のキーワードは文字を揃える位置を指定します。  

	- なし - 揃えません  
	- `C` - [`PRINTC`](./PRINTC.md)と同様に右側に揃えます  
	- `LC` - [`PRINTLC`](./PRINTC.md)と左に揃えます  

!!! hint "ヒント"

    命令のみ対応しています。
