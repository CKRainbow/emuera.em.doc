---
hide:
  - toc
---

# QUIT_AND_RESTART

| 函数名                                                                       | 参数   | 返回值 |
| :--------------------------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`QUIT_AND_RESTART`](./QUIT_AND_RESTART.md) | `void` | `void` |

!!! info "API"

    ``` { #language-erbapi }
    QUIT_AND_RESTART
    ```

    和 `QUIT` 一样，插入 WAIT 后重启 Emuera。

!!! hint "提示"

    命令 / 行内函数两种写法均有效。
