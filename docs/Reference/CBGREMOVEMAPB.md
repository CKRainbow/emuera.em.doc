---
hide:
  - toc
---

# CBGREMOVEMAPB

| 関数名                                                                     | 引数 | 戻り値 |
| :------------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`CBGREMOVEMAPB`](./CBGREMOVEMAPB.md) | なし | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int CBGREMOVEMAPB
    ```
	[`CBGSETBMAPG`](./CBGSETBMAPG.md)命令で設定したボタンマップの設定を解除します。

!!! hint "ヒント"

    命令、式中関数両方対応しています。
