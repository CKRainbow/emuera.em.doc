---
hide:
  - toc
---

# STRDATA

| 関数名                                                         | 引数             | 戻り値 |
| :------------------------------------------------------------- | :--------------- | :----- |
| ![](../assets/images/IconEmuera.webp)[`STRDATA`](./STRDATA.md) | `stringVariable` | なし   |

!!! info "API"

    ```  { #language-erbapi }
	STRDATA stringVariable
		DATA
		DATAFORM
		DATALIST
		ENDLIST
	ENDDATA
    ```
	[`PRINTDATA`](./PRINTDATA.md)で文字列を表示する代わりに、指定の文字列型変数に代入する命令です。  
	各書式は`PRINTDATA`と同じですのでそちらのページを参照してください。  

!!! hint "ヒント"

    命令のみ両方対応しています。
