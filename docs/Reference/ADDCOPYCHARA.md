---
hide:
  - toc
---

# ADDCOPYCHARA

| 関数名                                                                   | 引数 | 戻り値 |
| :----------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`ADDCOPYCHARA`](./ADDCOPYCHARA.md) | `int`| なし   |

!!! info "API"

    ```  { #language-erbapi }
	ADDCOPYCHARA charaID
    ```
	引数で指定された登録番号のキャラと同じデータであるキャラを新たに追加します。つまり、`ADDCHARA`の変種です。

!!! hint "ヒント"

    命令のみ対応しています。
