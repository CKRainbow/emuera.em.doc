---
hide:
  - toc
---

# ADDVOIDCHARA

| 関数名                                                                   | 引数 | 戻り値 |
| :----------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`ADDVOIDCHARA`](./ADDVOIDCHARA.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	ADDVOIDCHARA
    ```
	csvに依らずにキャラを追加する命令です。  
	`ADDVOIDCHARA`で追加されたキャラは全ての変数に`0`又は空文字列が代入されています。  

!!! hint "ヒント"

    命令のみ対応しています。
