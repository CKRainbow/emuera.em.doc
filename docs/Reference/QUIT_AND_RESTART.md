---
hide:
  - toc
---

# QUIT_AND_RESTART

| 関数名                                                                       | 引数   | 戻り値 |
| :--------------------------------------------------------------------------- | :----- | :----- |
| ![](../assets/images/IconEE.webp)[`QUIT_AND_RESTART`](./QUIT_AND_RESTART.md) | `void` | `void` |

!!! info "API"

    ``` { #language-erbapi }
	QUIT_AND_RESTART
    ```

	QUIT同様にWAITを挟んだ後にEmueraを再起動する

!!! hint "ヒント"

	命令なので式中関数としては使用できません
