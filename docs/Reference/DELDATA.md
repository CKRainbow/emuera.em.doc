---
hide:
  - toc
---

# DELDATA

| 関数名                                                         | 引数 | 戻り値 |
| :------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`DELDATA`](./DELDATA.md) | `int`| なし   |

!!! info "API"

    ```  { #language-erbapi }
	DELDATA saveID
    ```
	`saveID`で示される番号のファイルのデータを削除します。  
	ファイルが存在しなくてもエラーにはなりません。  

!!! hint "ヒント"

    命令のみ対応しています。
