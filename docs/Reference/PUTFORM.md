---
hide:
  - toc
---

# PUTFORM

| 関数名                                                           | 引数       | 戻り値 |
| :--------------------------------------------------------------- | :--------- | :----- |
| ![](../assets/images/Iconeramaker.webp)[`PUTFORM`](./PUTFORM.md) | `string`   | なし   |

!!! info "API"

    ```  { #language-erbapi }
	PUTFORM saveInfo
    ```
    `PUTFORM`は`@SAVEINFO`という特殊な関数でのみ使えます。  
	`PRINTFORM`と同様の書式で書くことにより、セーブデータに概要をつけることができます。  
	何日目か、キャラの能力はどれくらいか、どのキャラを調教しているかなどのデータを書き込むとよいでしょう。


!!! hint "ヒント"

    命令のみ両方対応しています。
