# Shortcut keys  
In Emuera, you can perform some operations with shortcut keys.  

## Ctrl + B  
Minimize the Emuera window.  

## Ctrl + C, Ctrl + Insert  
Open the clipboard dialog.  
This operation is valid only when no character is selected.  
If any character is selected, just copy that character.  

## Ctrl + V, Shift + Insert  
Paste the copied characters.  

## Ctrl + D  
Open the debug window. This is valid only when started in debug mode.  

## Ctrl + R  
Updates information in the debug window. Focus does not shift.  
This is valid only when started in debug mode.  

## Ctrl + 0-9  
Switches the macro group to groups 0 to 9.  

## Shift + F1-F12  
Assigns the current string to F1 to F12 as a macro.  

## F1-F12  
Call the macro assigned to each key.  

## Esc  
Press during a macro execution to interrupt the macro.  
